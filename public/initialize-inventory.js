// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict';

  window.addEventListener('load', function () {
    // Set up FeathersJS app
    var app = feathers();
    
    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client 
    app.configure(restClient.fetch(window.fetch));

    // Connect to the `inventory` service
    const mid2inventory = app.service('mid-2-inventory');

    var initButton = document.getElementById('initbutton');

    initButton.addEventListener('click',function(event){
        // clear out old inventory
        mid2inventory.remove( null, {
            query: {}
        }).then(function(){
            // add new products
            mid2inventory.create({
                description: "shirt",
                unitPrice: 29.95,
                quantity: 100
            });
            mid2inventory.create({
                description: "watch",
                unitPrice: 1299.95,
                quantity: 5
            });
            // ADD 5 MORE ITEMS TO THE INVENTORY HERE
            mid2inventory.create({
                description: "laptop",
                unitPrice: 899.90,
                quantity: 10
            });
            mid2inventory.create({
                description: "backpack",
                unitPrice: 80.95,
                quantity: 30
            });
            mid2inventory.create({
                description: "medical mask",
                unitPrice: 60.95,
                quantity: 80
            });
            mid2inventory.create({
                description: "jacket",
                unitPrice: 129.99,
                quantity: 120
            });
            mid2inventory.create({
                description: "pants",
                unitPrice: 45.95,
                quantity: 150
            });
        }); 
    });
  }, false);
}());
