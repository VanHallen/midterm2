// DEFINE ANY NEW FUNCTIONS YOU NEED HERE
// Shows the items
const showInventory = async inventoryService => {
    $('#products tbody tr').remove();
    // Find the items
    const inventory = await inventoryService.find();
    inventory.data.forEach(addItem);
}
// Adds an item row to the table
const addItem = item => {
  $('#products > tbody:last-child').append(
    `<tr>
      <td>${item.description}</td>
      <td>${item.unitPrice}</td>
      <td><input class="quantity" data-id=${item._id} type="number" name="quantity" min="0" value="0"></td>
    </tr>`
  );
};

const getOrder = () => {
  console.log("Placing order...")
  let order = [];
  $('.quantity').each((i, quantity) => {
    let id = $(quantity).data('id');
    let q = parseInt($(quantity).val());
    order.push({
      id: id,
      quantity: q
    })
  });
  return order;
}

(function () {
  'use strict';

  window.addEventListener('load', function () {
    // Set up FeathersJS app
    var app = feathers();
    
    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client 
    app.configure(restClient.fetch(window.fetch));

    // ADD ANY ADDITIONAL CODE YOU NEED HERE
    // Connect to the `mid2order` service
    const orderService = app.service('mid-2-order');
    // Connect to mid2inventory service
    const inventoryService = app.service('mid-2-inventory');
    // Connect to mid2orderline service
    const orderlineService = app.service('mid-2-orderline');
    // Show existing inventory in the table
    showInventory(inventoryService);
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation')

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
      event.preventDefault();
      event.stopPropagation();

      const order = getOrder();
      if (form.checkValidity()) {
        orderService.create({
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            email: $('#email').val(),
        }).then(function() {
          form.classList.remove('was-validated');
          form.reset();
        });

        order.forEach(product => {
          if(product.quantity > 0) {
            orderlineService.create({
              itemId: product.id,
              quantity: product.quantity
            });
          }
        });

      } else {
        form.classList.add('was-validated')
      }
      event.preventDefault()
      event.stopPropagation()
      }, false)
    })


  }, false);
}());
