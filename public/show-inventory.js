// DEFINE ANY NEW FUNCTIONS YOU NEED HERE
// Shows the items
const showInventory = async mid2inventory => {
    $('#products tbody tr').remove();
    // Find the items
    const inventory = await mid2inventory.find();
    inventory.data.forEach(addItem);
}
// Adds a signup row to the table
const addItem = item => {
  $('#products > tbody:last-child').append(
    `<tr>
      <td>${item._id}</td>
      <td>${item.description}</td>
      <td>${item.unitPrice}</td>
      <td>${item.quantity}</td>
    </tr>`
  );
};

(function () {
  'use strict';

  window.addEventListener('load', function () {
    // Set up FeathersJS app
    var app = feathers();
    
    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client 
    app.configure(restClient.fetch(window.fetch));

    // ADD ANY ADDITIONAL CODE YOU NEED HERE
    // Connect to service
    const mid2inventory = app.service('mid-2-inventory');
    // Show existing signups in the table
    showInventory(mid2inventory);
  }, false);
}());
