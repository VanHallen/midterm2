//import app from '../../src/app';
import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'mid2inventory\' service', () => {
  let mongoServer;
  let app;

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = appFunc();
  });

  afterAll(async () => {
    await mongoServer.stop();
  });

  it('registered the service', async () => {
    const service = app.service('mid-2-inventory');
    expect(service).toBeTruthy();
  });

  it('initializes inventory with a product', async () => {
    const product = await app.service('mid-2-inventory').create({
      description: "shirt",
      unitPrice: 29.95,
      quantity: 100
    });
    expect(product.description).toBe("shirt");
    expect(product.unitPrice).toBe(29.95);
    expect(product.quantity).toBe(100);
  }
});
