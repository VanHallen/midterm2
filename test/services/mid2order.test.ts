//import app from '../../src/app';
import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'mid2order\' service', () => {
  let mongoServer;
  let app;

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = appFunc();
  });

  afterAll(async () => {
    await mongoServer.stop();
  });
  it('registered the service', () => {
    const service = app.service('mid-2-order');
    expect(service).toBeTruthy();
  });

  it('creates an order for the user', async () => {
    const order = await app.service('mid-2-order').create({
      firstName: 'Bob',
      lastName: 'Smith',
      email: 'bobsmith@email.com'
    });
    expect(order).toBeTruthy();
  });
});
