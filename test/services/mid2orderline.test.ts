//import app from '../../src/app';
import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'mid2orderline\' service', () => {
  let mongoServer;
  let app;
  let product;
  let inventoryService;

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = await appFunc();
    inventoryService = app.service('mid-2-inventory');
    product = await inventoryService.create({
      description: "hat",
      unitPrice: 29.95,
      quantity: 5
    });
  });

  afterAll(async () => {
    await mongoServer.stop();
  });

  it('registered the service', () => {
    const service = app.service('mid-2-orderline');
    expect(service).toBeTruthy();
  });

  it('places order and decreases quantity in inventory', async () => {
    let products = await inventoryService.find();
    const orderPlaced = await app.service('mid-2-orderline').create({
      itemId: products.data[0]._id,
      quantity: 3
    });
    products = await inventoryService.find();
    expect(orderPlaced).toBeTruthy();
    expect(products.data[0].quantity).toEqual(2);
  });

  it('refuses order if there are not enough units', async () => {
    const products = await inventoryService.find();
    await expect(async () => { const order = await app.service('mid-2-orderline').create({
      itemId: products.data[0]._id,
      quantity: 5
    })}).rejects.toThrow(Error);
  });
});