// Initializes the `mid2inventory` service on path `/mid-2-inventory`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Mid2Inventory } from './mid2inventory.class';
import createModel from '../../models/mid2inventory.model';
import hooks from './mid2inventory.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'mid-2-inventory': Mid2Inventory & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate'),
    multi: [ 'remove' ]
  };

  // Initialize our service with any options it requires
  app.use('/mid-2-inventory', new Mid2Inventory(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('mid-2-inventory');

  service.hooks(hooks);
}
