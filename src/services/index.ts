import { Application } from '../declarations';
import mid2Inventory from './mid2inventory/mid2inventory.service';
import mid2Order from './mid2order/mid2order.service';
import mid2Orderline from './mid2orderline/mid2orderline.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(mid2Inventory);
  app.configure(mid2Order);
  app.configure(mid2Orderline);
}
