// Initializes the `mid2order` service on path `/mid-2-order`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Mid2Order } from './mid2order.class';
import createModel from '../../models/mid2order.model';
import hooks from './mid2order.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'mid-2-order': Mid2Order & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/mid-2-order', new Mid2Order(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('mid-2-order');

  service.hooks(hooks);
}
