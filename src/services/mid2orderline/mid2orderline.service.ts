// Initializes the `mid2orderline` service on path `/mid-2-orderline`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Mid2Orderline } from './mid2orderline.class';
import createModel from '../../models/mid2orderline.model';
import hooks from './mid2orderline.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'mid-2-orderline': Mid2Orderline & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/mid-2-orderline', new Mid2Orderline(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('mid-2-orderline');

  service.hooks(hooks);
}
