
import checkOrder from '../../hooks/check-order';
import removeItem from '../../hooks/remove-item';
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [checkOrder()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [removeItem()],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
