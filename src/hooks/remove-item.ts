// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {

    const {app, data} = context;

    const products = await app.service('mid-2-inventory').find({
      query: {
        _id: data.itemId
      }
    });

    app.service('mid-2-inventory').patch(data.itemId, {
      quantity: products.data[0].quantity - data.quantity
    });

    return context;
  };
};
