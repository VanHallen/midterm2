// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {

    const {app, data} = context;
    const inventoryService = app.service('mid-2-inventory');

    const product = await inventoryService.find({
      query: {
        _id: data.itemId
      }
    });

    const productInInventory = product.data[0];
    if (data.quantity > productInInventory.quantity) {
      throw new Error(`There are only ${productInInventory.quantity} units of ${productInInventory.description}`);
    }
    return context;
  };
};
